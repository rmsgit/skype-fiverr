var express = require('express');
var router = express.Router();
var send = require('../app_basic/send')
var users = require('../model/users');
var instrument = require('../model/instrument');
var token = require('../app_basic/token');
var user_roles = require('../app_basic/user_role')

//external module
var email_validator = require("email-validator");

var generalUser = users.generalUser;
var data_entry  = users.dataEntry;
var shop_owner  = users.shopOwner;


/*
* save general user
* */
router.post('/general_user', function(req, res, next) {


    //chk email enterd in correct or not
    if (!email_validator.validate(req.body.email)) {
        send.data(req, res, {
            message: 'email not valid'
        });
    }
    else {
        var usr_email = generalUser.find({email: req.body.email});
        var query = {'email': req.body.email};
        generalUser.find(query, function (err, result) {
            if (err) {
                send.error(req, res, 5001);

            }else if (result.length === 1 ){
                send.data(req, res,{message: 'E-mail is already using'} )
            }
            else{
                var newToken = token.new_token('First', user_roles.general);

                new generalUser({
                    name   : req.body.name,
                    email  : req.body.email,
                    imgUrl : req.body.imgUrl,
                    password : req.body.password,
                    role : user_roles.general,
                    token: newToken

                }).save(function (err, doc) {
                    if (err){
                        send.error(req, res, 5001);
                    }
                    else {
                        send.data(req, res, {
                            user_role: user_roles.general ,
                            token: newToken
                        });
                    }
                });
            }})
    }
});
router.post('/instrument', function(req, res, next) {
    
    new instrument(req.body)
    .save(function (err, doc) {
        if (err){
            send.error(req, res, 5001);
        }
        else {
            send.data(req, res, "Saved");
        }
    });

});

router.post('/instrument/:id', function(req, res, next) {
    
    instrument.update(
        {_id:req.params['id']}, 
        {$set: req.body},
        function (err, doc) {
        if (err){
            send.error(req, res, 5001);
        }
        else {
            send.data(req, res, "Saved");
        }
    });

});


module.exports = router;