var errors = require('./erros')
module.exports = {
  data:function (req, res, data) {
      res.setHeader('Content-Type', 'application/json');
      // Website you wish to allow to connect
      res.setHeader('Access-Control-Allow-Origin', '*');
      /*
      // Request methods you wish to allow
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

      // Request headers you wish to allow
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

      // Set to true if you need the website to include cookies in the requests sent
      // to the API (e.g. in case you use sessions)
      res.setHeader('Access-Control-Allow-Credentials', true);*/
      res.send(JSON.stringify({
          status: 200,
          message: "SUCCESS",
          data: data
      }));
  },
  error:function (req, res, errorCode) {
        res.setHeader('Content-Type', 'application/json');
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');
        /*
         // Request methods you wish to allow
         res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

         // Request headers you wish to allow
         res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

         // Set to true if you need the website to include cookies in the requests sent
         // to the API (e.g. in case you use sessions)
         res.setHeader('Access-Control-Allow-Credentials', true);*/
        var errorMessage = errors(errorCode)
        res.status(errorMessage.status)
        res.send(JSON.stringify(errorMessage));
    },


};