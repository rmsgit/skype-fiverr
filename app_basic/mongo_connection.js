/**
 * Created by Ruwan on 12/16/2017.
 */
// var MongoClient = require('mongodb').MongoClient
//     , assert = require('assert');
var url = 'mongodb://localhost:27017/instrument';
// var mongo_db;
// MongoClient.connect(url, function(err, db) {
//     assert.equal(null, err);
//     mongo_db = db;
//     console.log("Connected correctly to server");
//
//     db.close();
// });
var mongoose = require('mongoose');
mongoose.connect(url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("MongoDB connected")
});
module.exports = mongoose ;