/**
 * Created by Ruwan on 11/17/2017.
 */
const tables =  {
    customer: 'customer',
    data_entry: 'dummy',
    dummy: 'dummy',
    item: 'item',
    item_shop: 'item_shop',
    shop: 'shop',
    user: 'user'
};
var select = {
    all:'SELECT * ',
    cols:function (cols) {
        return 'SELECT '+ cols.join(', ')
    }
};
var from = function (table) {
    return 'FROM ' + table;
};

var where = function (where) {
    return 'WHERE ' + where;
};
var update = function (table, sets) {
    return 'UPDATE  '+ table + ' SET '+ sets;
};
var query = function (q) {
    var query  =q.join(' ');
    return query;
};
module.exports = {
    user: {
        all:query([
            select.all,
            from(tables.user)
        ]),
        form_email:function (email){
            return query([
                select.all,
                from(tables.user),
                where('email = "'+ email + '"')
            ])
        },
        form_token:function (token){
            return query([
                select.all,
                from(tables.user),
                where('token = "'+ token + '"')
            ])
        },
        set_token: function (token, user_id) {
            return query([
                update(tables.user, 'token = "'+ token + '"' ),
                where('id ='+ user_id)
            ])
        }
    }
};