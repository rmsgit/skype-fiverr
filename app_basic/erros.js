var errors = {
    /**
     * Http Errors
     * */
    404: "NOT FOUND",
    400: "BAD REQUEST",
    500: "INTERNAL SERVER ERROR",

    /**
     * Internal Server  errors
     * */
    5001:"DATABASE ERROR"
};
module.exports = function (errorCode) {
    if (errorCode > 1000){
        return {
            status: 500,
            message: errors[500],
            internal_details: {
                error_code:errorCode,
                error_message: errors[errorCode]
            }
        }
    }else {
        return {
            status: errorCode,
            message: errors[errorCode]
        }
    }

}
