/**
 * @author Jaspreet Chahal
 * @version 0.5
 * Elastic Text box
 * Auto grow text boxes or fit text
 * License Copyright 2012 Jaspreet Chahal
 http://jaspreetchahal.org/

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * Tested on IE9,IE10, FF 10+, Chrome, Safari 4+, Opera 11+
 */

jQuery.fn.jcElasticTextBox = function(settings) {
    var settings = jQuery.extend(
        {
            mode:'fit', // grow || fit
            // grow options
            maxGrowWidth: 400,
            minGrowWidth: 200,
            roomToRight:10,
            // fit options
            minFitFontSize: 11,
            // animation options
            easing:true,
            easingInterval:150 //milliseconds (lower value recommended)
        },
        settings);
    if(jQuery("#jcTextFitGrowCanvas").length == 0) {
        jQuery('<canvas id="jcTextFitGrowCanvas"  width="300" height="300" style="display: none;"></canvas>').appendTo('body');
    }
    return this.each(function(){
        var canvas = document.getElementById("jcTextFitGrowCanvas");
        var canvasContext = canvas.getContext("2d");
        jQuery(this).width(settings.minGrowWidth || jQuery(this).width());
        var currentWidth = jQuery(this).width();
        var currentHeight = parseInt(jQuery(this).height());
        var currentFontSize = parseInt(jQuery(this).css("fontSize").replace("px",''));
        var originalFontSize = currentFontSize;
        // CSS3 transitions

        jQuery(this).css({height: currentHeight});

        var fontSize = jQuery(this).css("fontSize");
        var fontFamily = jQuery(this).css("fontFamily");
        canvasContext.font = parseInt(fontSize.replace('px',''))+"px "+fontFamily;
        canvasContext.fillStyle = "black";

        jQuery(this).keydown(function(e){
            if(settings.mode == 'grow') {
                textWidth = canvasContext.measureText(jQuery(this).val());
                if(textWidth.width > currentWidth && textWidth.width < settings.maxGrowWidth) {
                    if(settings.easing) {
                        jQuery(this).stop().animate({width: textWidth.width+settings.roomToRight},settings.easingInterval);
                    }
                    else {
                        jQuery(this).width(textWidth.width);
                    }
                }
                if(textWidth.width - settings.roomToRight <= currentWidth) {
                    jQuery(this).width(currentWidth);
                }
            }
            else if(settings.mode == 'fit') {
                canvasContext.fillText(jQuery(this).val(),0,0);
                textWidth = canvasContext.measureText(jQuery(this).val());
                jQuery(this).css({height: currentHeight});
                if(textWidth.width > currentWidth && currentFontSize > settings.minFitFontSize) {
                    if(settings.easing) {
                        jQuery(this).stop().animate({fontSize: --currentFontSize},settings.easingInterval);
                    }
                    else {
                        jQuery(this).css({fontSize: --currentFontSize});
                    }
                }
                else if(textWidth.width < currentWidth) {
                    currentFontSize = originalFontSize;
                    if(settings.easing) {
                        jQuery(this).stop().animate({fontSize: originalFontSize},settings.easingInterval);
                    }
                    else {
                        jQuery(this).css({fontSize: originalFontSize});
                    }
                }
            }
        });
    });
};
