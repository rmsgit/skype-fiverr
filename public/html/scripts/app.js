var app = app || {};

(function ($, plugin) {
	'use strict';

    // ie
    if ( !!navigator.userAgent.match(/MSIE/i) || !!navigator.userAgent.match(/Trident.*rv:11\./) ){
      $('body').addClass('ie');
    }

    // iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
    var ua = window['navigator']['userAgent'] || window['navigator']['vendor'] || window['opera'];
    if( (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua) ){
        $('body').addClass('touch');
    }

    // fix z-index on ios safari
    if( (/iPhone|iPod|iPad/).test(ua) ){
      $(document, '.modal, .aside').on('shown.bs.modal', function(e) {
        var backDrop = $('.modal-backdrop');
        $(e.target).after($(backDrop));
      });
    }

    //resize
    $(window).on('resize', function () {
      var $w = $(window).width()
          ,$lg = 1200
          ,$md = 991
          ,$sm = 768
          ;
      if($w > $lg){
        $('.aside-lg').modal('hide');
      }
      if($w > $md){
        $('#aside').modal('hide');
        $('.aside-md, .aside-sm').modal('hide');
      }
      if($w > $sm){
        $('.aside-sm').modal('hide');
      }
    });

    app.init = function () {

      $('[data-toggle="popover"]').popover();
      $('[data-toggle="tooltip"]').tooltip();

      // init the plugin
      $('body').find('[data-plugin]').plugin();

    }
    
   	app.init();

   	$(document).on('pjaxEnd', function(){
   		app.init();
   	});

    function elasticArea() {
    $('.js-elasticArea').each(function(index, element) {
       var elasticElement = element,
          $elasticElement = $(element),
          initialHeight = initialHeight || $elasticElement.height(),
          delta = parseInt( $elasticElement.css('paddingBottom') ) + parseInt( $elasticElement.css('paddingTop') ) || 0,
          resize = function() {
            $elasticElement.height(initialHeight);
            $elasticElement.height( elasticElement.scrollHeight - delta );
        };
      
      $elasticElement.on('input change keyup', resize);
      resize();
    });
    
  };

  //Init function in the view
  elasticArea();

})(jQuery, app);

$(document).ready(function(){
      var im = new Inputmask("9 999 999-99-99");
      im.mask($('.phone_masking'));
      im.mask($('.phone_with_country_flag'));
      //$('.email_masking').inputmask();
      //$('.website_masking').inputmask();
      jQuery(".grow").jcElasticTextBox();
      jQuery(".grow1").jcElasticTextBox({mode:'grow',maxGrowWidth:511});
      jQuery(".grow2").jcElasticTextBox({mode:'grow',maxGrowWidth:800});
      console.log(jQuery(".lead").jcDetectTextWidth());
      $(".exampleInputColorPicker").colorpicker();
      $('.dropify').dropify();

      $(".summernote").summernote();
      $('.small-texteditor').froalaEditor({
        'height' : '80px',
         toolbarButtons: ['bold', 'italic', 'underline']
      });
      $('.medium-texteditor').froalaEditor({
        toolbarButtons: ['show', 'bold', 'italic', 'underline', 'strikeThrough', 'indent', 'outdent', 'undo', 'redo', 'insertImage', 'createLink']
      });
      $('.large-texteditor').froalaEditor({
        'height' : '200px'
      });
      $(".phone_with_country_flag").intlTelInput();

      $('.datepicker').datepicker({
          format: 'dd/mm/yyyy',
          todayHighlight:'TRUE',
          autoclose: true,
      })

      $('#btn-single-file-upload').on('click',function()
      {
        $("#input-single-file-upload").click();
      });

      $('#btn-single-file-upload-with-title-change').on('click',function()
      {
        $("#input-single-file-upload-with-title-change").click();
      });

      $('#btn-single-file-upload-style-b').on('click',function()
      {
        $("#input-single-file-upload-style-b").click();
      });

      $('#btn-single-file-upload-with-title-change-style-b').on('click',function()
      {
        $("#input-single-file-upload-with-title-change-style-b").click();
      });

      /*$('.btn-multiple-file-upload-with-title-change').on('click',function()
      {
        alert("Hello");
        $(this).parent().parent().find(".input-multiple-file-upload-with-title-change").click();
      });*/

      $(".img-preview-container").hide();

      $(document).on('click','.btn-multiple-file-upload-with-title-change',function()
      {
        $(this).parent().parent().find(".input-multiple-file-upload-with-title-change").click();
      });      

      $("#input-single-file-upload").on('change',function()
      {
        readURL(this);
      });

      $("#input-single-file-upload-with-title-change").on('change',function()
      {
        readURL(this);
      });

      $("#input-single-file-upload-style-b").on('change',function()
      {
        readURL(this);
      });

      $("#input-single-file-upload-with-title-change-style-b").on('change',function()
      {
        readURL(this);
      });

      $(document).on("change",".input-multiple-file-upload-with-title-change",function()
      {
        readURL(this);
      });

      $(document).on("click",".btn-file-remove",function()
      {
        // console.log($(this).parent().parent().html());
        $(this).parent().parent().find('.img-file-name').html('');
        $(this).parent().parent().find('.img-file-name-text').val('');
        $(this).parent().parent().find('.img-preview-container').hide();
        $(this).parent().parent().find('.img-preview-container .img-file-upload-preview').attr('src','#');
        $(this).hide();
      });

      $(document).on("click",".btn-add-more-image-upload",function()
      {
        var imgUploadPicker = $(this).parent().parent().find(".multiple-image-upload-container:first").clone();
        var multiple_img_container = $(this).parent().parent().find('.multiple-image-upload-root-container');
        imgUploadPicker.find('.img-file-name').html('');
        imgUploadPicker.find('.img-file-name-text').val('');
        imgUploadPicker.find('.img-file-upload-preview').attr('src','');
        imgUploadPicker.find('.remove-image-upload-row').show();
        imgUploadPicker.find('.img-preview-container').hide();
        imgUploadPicker.find('.input-group').css('display','inline-flex');
        imgUploadPicker.find('.remove-image-upload-row').css('display','inherit');
        imgUploadPicker.appendTo(multiple_img_container).insertAfter(this);
        // $(".multiple-image-upload-root-container").append(imgUploadPicker);
      });

      $(document).on("click",".remove-image-upload-row",function()
      {
        $(this).parent().hide();
      });

      $('select').prop('selectedIndex',0);

          $('#multi_d').multiselect({
        right: '#multi_d_to, #multi_d_to_2',
        rightSelected: '#multi_d_rightSelected, #multi_d_rightSelected_2',
        leftSelected: '#multi_d_leftSelected, #multi_d_leftSelected_2',
        rightAll: '#multi_d_rightAll, #multi_d_rightAll_2',
        leftAll: '#multi_d_leftAll, #multi_d_leftAll_2',
 
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />'
        },
 
        moveToRight: function(Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id');
 
            if (button == 'multi_d_rightSelected') {
                var $left_options = Multiselect.$left.find('> option:selected');
                Multiselect.$right.eq(0).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(0).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(0));
                }
            } else if (button == 'multi_d_rightAll') {
                var $left_options = Multiselect.$left.children(':visible');
                Multiselect.$right.eq(0).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(0).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(0));
                }
            } else if (button == 'multi_d_rightSelected_2') {
                var $left_options = Multiselect.$left.find('> option:selected');
                Multiselect.$right.eq(1).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(1));
                }
            } else if (button == 'multi_d_rightAll_2') {
                var $left_options = Multiselect.$left.children(':visible');
                Multiselect.$right.eq(1).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(1).eq(1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(1));
                }
            }
        },
 
        moveToLeft: function(Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id');
 
            if (button == 'multi_d_leftSelected') {
                var $right_options = Multiselect.$right.eq(0).find('> option:selected');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_leftAll') {
                var $right_options = Multiselect.$right.eq(0).children(':visible');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_leftSelected_2') {
                var $right_options = Multiselect.$right.eq(1).find('> option:selected');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_leftAll_2') {
                var $right_options = Multiselect.$right.eq(1).children(':visible');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            }
        }
    });


  $('#multi_d_style_b').multiselect({
        right: '#multi_d_to_style_b, #multi_d_to_2_style_b',
        rightSelected: '#multi_d_style_b_rightSelected, #multi_d_style_b_rightSelected_2',
        leftSelected: '#multi_d_style_b_leftSelected, #multi_d_style_b_leftSelected_2',
        rightAll: '#multi_d_style_b_rightAll, #multi_d_style_b_rightAll_2',
        leftAll: '#multi_d_style_b_leftAll, #multi_d_style_b_leftAll_2',
 
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />'
        },
 
        moveToRight: function(Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id');
 
            if (button == 'multi_d_style_b_rightSelected') {
                var $left_options = Multiselect.$left.find('> option:selected');
                Multiselect.$right.eq(0).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(0).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(0));
                }
            } else if (button == 'multi_d_style_b_rightAll') {
                var $left_options = Multiselect.$left.children(':visible');
                Multiselect.$right.eq(0).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(0).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(0));
                }
            } else if (button == 'multi_d_style_b_rightSelected_2') {
                var $left_options = Multiselect.$left.find('> option:selected');
                Multiselect.$right.eq(1).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(1));
                }
            } else if (button == 'multi_d_style_b_rightAll_2') {
                var $left_options = Multiselect.$left.children(':visible');
                Multiselect.$right.eq(1).append($left_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$right.eq(1).eq(1).find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$right.eq(1));
                }
            }
        },
 
        moveToLeft: function(Multiselect, $options, event, silent, skipStack) {
            var button = $(event.currentTarget).attr('id');
 
            if (button == 'multi_d_style_b_leftSelected') {
                var $right_options = Multiselect.$right.eq(0).find('> option:selected');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_style_b_leftAll') {
                var $right_options = Multiselect.$right.eq(0).children(':visible');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_style_b_leftSelected_2') {
                var $right_options = Multiselect.$right.eq(1).find('> option:selected');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            } else if (button == 'multi_d_style_b_leftAll_2') {
                var $right_options = Multiselect.$right.eq(1).children(':visible');
                Multiselect.$left.append($right_options);
 
                if ( typeof Multiselect.callbacks.sort == 'function' && !silent ) {
                    Multiselect.$left.find('> option').sort(Multiselect.callbacks.sort).appendTo(Multiselect.$left);
                }
            }
        }
    });
      
  })

  function readURL(input) 
  {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $(input).parent().parent().find('.img-preview-container').show();
          $(input).parent().parent().find('.img-file-upload-preview').show();
          var filename = $(input).val().replace(/C:\\fakepath\\/i, '')
          $(input).parent().parent().find('.img-file-name').html(''+filename);
          $(input).parent().parent().find('.img-file-name-text').val(''+filename);
          $(input).parent().parent().find('.btn-file-remove').css('display','inherit');
          $(input).parent().parent().find('.img-file-upload-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
}

jQuery.fn.jcDetectTextWidth = function() {
  if(jQuery("#jcTextWidthDetect").length == 0) {
    jQuery('<canvas id="jcTextWidthDetect"  style="display: none;"></canvas>').appendTo('body');
  }
  var canvas = document.getElementById("jcTextFitGrowCanvas");
  var canvasContext = canvas.getContext("2d");
  var fontSize = jQuery(this).css("fontSize");
  var fontFamily = jQuery(this).css("fontFamily");
  if(canvasContext && false) {
    canvasContext.font = parseInt(fontSize.replace('px',''))+"px "+fontFamily;
    canvasContext.fillStyle = "black";
    return canvasContext.measureText(jQuery(this).text()).width;
  }
  else {
    jQuery('<div id="jcTextWidthDetectC"  style="display: none;"></div>').appendTo('body');
    jQuery("#jcTextWidthDetectC").css("fontSize",fontSize+'px');
    jQuery("#jcTextWidthDetectC").css("fontFamily",fontFamily);
    var text = jQuery(this).html();
    jQuery("#jcTextWidthDetectC").html(text);
    var width = jQuery("#jcTextWidthDetectC").width();
    jQuery("#jcTextWidthDetectC").remove();
    return width;
  }
};
