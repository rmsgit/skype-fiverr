$('.select-search').select2({ });
	
	// select 2 colored
	var oldclassback1="";
	var oldclasstext1="";
	$("#select2colored .dropdown-menu li").click(function(){
		var val_selected = $(this).find("div").html();
		$("#select2colored .multiselect-selected-text").html(val_selected);
		
		var selected_background = $(this).attr("class").split(" ")[0];
		$("#select2colored .custome-select").removeClass(oldclassback1);
		$("#select2colored .custome-select").addClass(selected_background);
		oldclassback1 = selected_background;
		
		var selected_text = $(this).attr("class").split(" ")[1];
		$("#select2colored .custome-select").removeClass(oldclasstext1);
		$("#select2colored .custome-select").addClass(selected_text);
		oldclasstext1 = selected_text;
	});
	
	var oldclassback2="";
	var oldclasstext2="";
	$("#select2colored2 .dropdown-menu li").click(function(){
		var val_selected = $(this).find("div").html();
		$("#select2colored2 .multiselect-selected-text").html(val_selected);
		
		var selected_background = $(this).attr("class").split(" ")[0];
		$("#select2colored2 .custome-select").removeClass(oldclassback2);
		$("#select2colored2 .custome-select").addClass(selected_background);
		oldclassback2 = selected_background;
		
		var selected_text = $(this).attr("class").split(" ")[1];
		$("#select2colored2 .custome-select").removeClass(oldclasstext2);
		$("#select2colored2 .custome-select").addClass(selected_text);
		oldclasstext2 = selected_text;
	});
	
	
	var oldclassback3="";
	var oldclasstext3="";
	$("#select2colored3 .dropdown-menu li").click(function(){
		var val_selected = $(this).find("div").html();
		$("#select2colored3 .multiselect-selected-text").html(val_selected);
		
		var selected_background = $(this).attr("class").split(" ")[0];
		$("#select2colored3 .custome-select").removeClass(oldclassback3);
		$("#select2colored3 .custome-select").addClass(selected_background);
		oldclassback3 = selected_background;
		
		var selected_text = $(this).attr("class").split(" ")[1];
		$("#select2colored3 .custome-select").removeClass(oldclasstext3);
		$("#select2colored3 .custome-select").addClass(selected_text);
		oldclasstext3 = selected_text;
	});

	var oldclassback_style_b_3="";
	var oldclasstext_style_b_3="";
	$("#select2colored_style_b_3 .dropdown-menu li").click(function(){
		var val_selected = $(this).find("div").html();
		$("#select2colored_style_b_3 .multiselect-selected-text").html(val_selected);
		
		var selected_background = $(this).attr("class").split(" ")[0];
		$("#select2colored_style_b_3 .custome-select").removeClass(oldclassback_style_b_3);
		$("#select2colored_style_b_3 .custome-select").addClass(selected_background);
		oldclassback_style_b_3 = selected_background;
		
		var selected_text = $(this).attr("class").split(" ")[1];
		$("#select2colored_style_b_3 .custome-select").removeClass(oldclasstext_style_b_3);
		$("#select2colored_style_b_3 .custome-select").addClass(selected_text);
		oldclasstext_style_b_3 = selected_text;
	});

	var oldclassback_style_b_2="";
	var oldclasstext_style_b_2="";
	$("#select2colored_style_b_2 .dropdown-menu li").click(function(){
		var val_selected = $(this).find("div").html();
		$("#select2colored_style_b_2 .multiselect-selected-text").html(val_selected);
		
		var selected_background = $(this).attr("class").split(" ")[0];
		$("#select2colored_style_b_2 .custome-select").removeClass(oldclassback_style_b_2);
		$("#select2colored_style_b_2 .custome-select").addClass(selected_background);
		oldclassback_style_b_2 = selected_background;
		
		var selected_text = $(this).attr("class").split(" ")[1];
		$("#select2colored_style_b_2 .custome-select").removeClass(oldclasstext_style_b_2);
		$("#select2colored_style_b_2 .custome-select").addClass(selected_text);
		oldclasstext_style_b_2 = selected_text;
	});


	var oldclassback_style_b_1="";
	var oldclasstext_style_b_1="";
	$("#select2colored_style_b_1 .dropdown-menu li").click(function(){
		var val_selected = $(this).find("div").html();
		$("#select2colored_style_b_1 .multiselect-selected-text").html(val_selected);
		
		var selected_background = $(this).attr("class").split(" ")[0];
		$("#select2colored_style_b_1 .custome-select").removeClass(oldclassback_style_b_1);
		$("#select2colored_style_b_1 .custome-select").addClass(selected_background);
		oldclassback_style_b_1 = selected_background;
		
		var selected_text = $(this).attr("class").split(" ")[1];
		$("#select2colored_style_b_1 .custome-select").removeClass(oldclasstext_style_b_1);
		$("#select2colored_style_b_1 .custome-select").addClass(selected_text);
		oldclasstext_style_b_1 = selected_text;
	});

	$(document).ready(function(){

		$(".toggle_menu label").click(function(e){
			$("#aside").toggleClass("active");
		});

		$('.panel [data-action=collapse]').click(function (e) {
            e.preventDefault();
            var $panelCollapse = $(this).parent().parent().parent().parent().nextAll();
            $(this).parents('.panel').toggleClass('panel-collapsed');
            $(this).toggleClass('rotate-180');

            containerHeight(); // recalculate page height

            $panelCollapse.slideToggle(150);
        });
        $('.panel [data-action=reload]').click(function (e) {
            e.preventDefault();
            var block = $(this).parent().parent().parent().parent().parent();
            $(block).block({ 
                message: '<i class="icon-spinner2 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait',
                    'box-shadow': '0 0 0 1px #ddd'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });

            // For demo purposes
            window.setTimeout(function () {
               $(block).unblock();
            }, 2000); 
        });
        $('.panel [data-action=close]').click(function (e) {
            e.preventDefault();
            var $panelClose = $(this).parent().parent().parent().parent().parent();

            containerHeight(); // recalculate page height

            $panelClose.slideUp(150, function() {
                $(this).remove();
            });
        });
        function containerHeight() {
            var availableHeight = $(window).height() - $('body > .navbar').outerHeight() - $('body > .navbar + .navbar').outerHeight() - $('body > .navbar + .navbar-collapse').outerHeight();

            $('.page-container').attr('style', 'min-height:' + availableHeight + 'px');
        }

		$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
    	$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
		$('#multi_d_upSelected').bind('click', function() {
	        $('#multi_d_to option:selected').each( function() {
	            var newPos = $('#multi_d_to option').index(this) - 1;
	            if (newPos > -1) {
	                $('#multi_d_to option').eq(newPos).before("<option value='"+$(this).val()+"' selected='selected'>"+$(this).text()+"</option>");
	                $(this).remove();
	            }
	        });
	    });
	    $('#multi_d_downSelected').bind('click', function() {
	        var countOptions = $('#multi_d_to option').size();
	        $('#multi_d_to option:selected').each( function() {
	            var newPos = $('#multi_d_to option').index(this) + 1;
	            if (newPos < countOptions) {
	                $('#multi_d_to option').eq(newPos).after("<option value='"+$(this).val()+"' selected='selected'>"+$(this).text()+"</option>");
	                $(this).remove();
	            }
	        });
	    });

	    $('#multi_d_style_b_upAll').bind('click', function() {
	        $('#multi_d_to_style_b option:selected').each( function() {
	            var newPos = $('#multi_d_to_style_b option').index(this) - 1;
	            if (newPos > -1) {
	                $('#multi_d_to_style_b option').eq(newPos).before("<option value='"+$(this).val()+"' selected='selected'>"+$(this).text()+"</option>");
	                $(this).remove();
	            }
	        });
	    });
	    $('#multi_d_style_b_downAll').bind('click', function() {
	        var countOptions = $('#multi_d_to_style_b option').size();
	        $('#multi_d_to_style_b option:selected').each( function() {
	            var newPos = $('#multi_d_to_style_b option').index(this) + 1;
	            if (newPos < countOptions) {
	                $('#multi_d_to_style_b option').eq(newPos).after("<option value='"+$(this).val()+"' selected='selected'>"+$(this).text()+"</option>");
	                $(this).remove();
	            }
	        });
	    });
		var conceptName = $('#aioConceptName').find(":selected").text();
		var im = new Inputmask("9 999 999-99-99");
		im.mask($('.phone_masking'));

				
				$('.email_masking').inputmask();
				$('.website_masking').inputmask();

				$('.summernote').summernote({
						  height: 300,                 // set editor height
						  minHeight: null,             // set minimum height of editor
						  maxHeight: null,             // set maximum height of editor
						  focus: true,
						                    // set focus to editable area after initializing summernote
				});
				$('.summernote-mediumEditor').summernote({
						                // set editor height
						  minHeight: null,             // set minimum height of editor
						  maxHeight: null,             // set maximum height of editor
						  focus: true,					// set focus to editable area after initializing summernote
						  toolbar: [
						    ['style', ['style']],
						    ['font', ['bold', 'italic', 'underline', 'clear']],
						    ['fontname', ['fontname']],
						    ['color', ['color']],
						    ['para', ['ul', 'ol', 'paragraph']],
						    
						    
						  ],                  
				});
				$('.summernote-smallEditor').summernote({
						                // set editor height
						  minHeight: null,             // set minimum height of editor
						  maxHeight: null,             // set maximum height of editor
						  focus: true,					// set focus to editable area after initializing summernote
						  toolbar: [
						    ['style', ['style']],
						    ['font', ['bold', 'italic', 'underline', 'clear']],
						    ['fontname', ['fontname']],
						    
						    
						    
						  ],                  
				});
				
				$('.summernote-wrapper').find('span.caret').removeClass('caret');
				$('.datepicker').datepicker({
					autoclose: true,
				});
				$('#timepicker').timepicker({
					autoclose: true,
				});
			});