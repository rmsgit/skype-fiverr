/**
 * Created by Ruwan on 12/17/2017.
 */


var mongoose = require('../app_basic/mongo_connection');

var usersCollection = 'users';

var generalUser = new mongoose.Schema({
    name : String,
    imgUrl : String,
    email : String,
    password : String,
    role : String,
    token : String
});


var dataEntry = new mongoose.Schema({
    shop_id : String,
    name : String,
    password: String,
    role : String,
    token : String
});

var shopOwner = new mongoose.Schema({
    name  : String,
    email : String,
    password: String,
    role : String,
    token : String
});

var general_user = mongoose.model(usersCollection, generalUser);
var data_Entry = mongoose.model(usersCollection, generalUser);
var shopOwner = mongoose.model(usersCollection, generalUser);

module.exports = {
    generalUser: general_user,
    dataEntry: data_Entry,
    shopOwner: shopOwner
}